function extend() {
    var target = {};
    for (var i = 0; i < arguments.length; i++) {
    var source = arguments[i];
    for (var key in source) {
        if (source.hasOwnProperty(key)) {
        target[key] = source[key];
        }
    }
    }
    return target;
}

// Optional libraries used to extend on reve./utils/revealal.js
var deps = [
    { src: 'https://gitlab.com/jibe-b/cdn/-/raw/master/utils/reveal/js/reveal-custom.js/utils/reveal/plugin/markdown/marked.js', condition: function() { return !!document.querySelector('[data-markdown]'); } },
    { src: 'https://gitlab.com/jibe-b/cdn/-/raw/master/utils/reveal/js/reveal-custom.js/utils/reveal/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector('[data-markdown]'); } },
    { src: 'https://gitlab.com/jibe-b/cdn/-/raw/master/utils/reveal/js/reveal-custom.js/utils/reveal/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
    { src: 'https://gitlab.com/jibe-b/cdn/-/raw/master/utils/reveal/js/reveal-custom.js/utils/reveal/plugin/zoom-js/zoom.js', async: true },
    { src: 'https://gitlab.com/jibe-b/cdn/-/raw/master/utils/reveal/js/reveal-custom.js/utils/reveal/plugin/notes/notes.js', async: true },
    { src: 'https://gitlab.com/jibe-b/cdn/-/raw/master/utils/reveal/js/reveal-custom.js/utils/reveal/plugin/math/math.js', async: true }
];

// default options to init reveal.js
var defaultOptions = {
    controls: true,
    progress: true,
    history: true,
    center: true,
    transition: 'default', // none/fade/slide/convex/concave/zoom
    dependencies: deps
};

// options from URL query string
var queryOptions = Reveal.getQueryHash() || {};

var options = extend(defaultOptions, {}, queryOptions);
